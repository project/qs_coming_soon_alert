<?php

namespace Drupal\qs_coming_soon_alert\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ComingSoonAlertConfigForm.
 */
class ComingSoonAlertConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'qs_coming_soon_alert_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('qs_coming_soon_alert.settings');
    $form['show_on_off'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show on/off '),
      '#description' => $this->t('This will allow the coming soon alerts to show or hide on frontend.'),
      '#default_value' => $config->get('show_on_off'),
    ];

    $form['title_cn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title (China)'),
      '#required' => TRUE,
      '#default_value' => $config->get('title_cn'),
      '#description' => '<p>' . $this->t(
          'Enter title in china language.'
        ) . '<br><br></p>',
    ];

    $form['message_cn'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message (China)'),
      '#required' => TRUE,
      '#default_value' => $config->get('message_cn'),
      '#description' => '<p>' . $this->t(
          'Enter message in China language.'
        ) . '<br><br></p>',
    ];

    $form['title_en'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title (English)'),
      '#required' => TRUE,
      '#default_value' => $config->get('title_en'),
      '#description' => '<p>' . $this->t(
          'Enter title in english language.'
        ) . '<br><br></p>',
    ];

    $form['message_en'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message (English)'),
      '#required' => TRUE,
      '#default_value' => $config->get('message_en'),
      '#description' => '<p>' . $this->t(
          'Enter message in English language.'
        ) . '<br><br></p>',
    ];

    // Organize the limit by pages options.
    $form['limit_by_pages_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Page Visibility'),
      //'#description' => $this->t('Limit the alert to only show on some of pages.'),
    ];

    $form['limit_by_pages_fieldset']['limit_to_pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#required' => TRUE,
      '#default_value' => ($config->get('limit_to_pages')) ?? '/',
      '#description' => '<p>' . $this->t(
          'Specify pages by using their paths. Enter one path per line. The \'*\' character is a wildcard. An example path is /user/* for every user page. / is the front page.'
        ) . '<br><br></p>',
    ];


    $form['limit_by_pages_fieldset']['limit_to_pages_negate'] = [
      '#type' => 'radios',
      '#title' => t('Negate for listed pages'),
      '#required' => TRUE,
      '#default_value' => $config->get('limit_to_pages_negate'),
      '#options' => [
        'show' => 'Show for the listed pages',
        'hide' => 'Hide for the listed pages',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    $this->config('qs_coming_soon_alert.settings')
      ->set('show_on_off', $form_state->getValue('show_on_off'))
      ->set('title_cn', $form_state->getValue('title_cn'))
      ->set('message_cn', $form_state->getValue('message_cn'))
      ->set('title_en', $form_state->getValue('title_en'))
      ->set('message_en', $form_state->getValue('message_en'))
      ->set('limit_to_pages', $form_state->getValue('limit_to_pages'))
      ->set('limit_to_pages_negate', $form_state->getValue('limit_to_pages_negate'))
      ->save();

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'qs_coming_soon_alert.settings',
    ];
  }

}
