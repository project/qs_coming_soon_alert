<?php

namespace Drupal\qs_coming_soon_alert\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'QS Coming soon alert' block.
 *
 * @Block(
 *   id = "qscomingsoon",
 *   admin_label = @Translation("QS Coming Soon Alert"),
 * )
 */
class QSComingSoon extends BlockBase {

  /**
   * {@inheritdoc}
   *
   * The return value of the build() method is a renderable array. Returning an
   * empty array will result in empty block contents. The front end will not
   * display empty blocks.
   */
  public function build() {
    $visibility = $this->block_acc();
    $language = Drupal::languageManager()->getCurrentLanguage()->getId();
    $title = $message = '';
    $config = Drupal::config('qs_coming_soon_alert.settings');
    $show = $config->get('show_on_off');
    if ($show == 1 && $visibility) {
      $title = ($language == 'cn') ? $config->get('title_cn') : $config->get('title_en');
      $message = ($language == 'cn') ? $config->get('message_cn') : $config->get('message_en');
    }

    return [
      '#theme' => 'qs_coming_soon_alert',
      '#show' => ($show == 1 && $visibility) ? 1 : '',
      '#heading' => $title,
      '#message' => $message,
    ];
  }

  public function block_acc() {
    $nodeid = Drupal::service('path.current')->getPath();
    $path = explode('/', trim(Drupal::request()->query->get('q'), '/'));
    if ($path[0] == "" && Drupal::service('path.matcher')
        ->isFrontPage() != TRUE) {
      $path = explode('/', trim(Drupal::service('path_alias.manager')
        ->getAliasByPath($nodeid), '/'));
    }
    $language = Drupal::languageManager()->getCurrentLanguage()->getId();

    // - unset language id if present in path.
    if ($path[0] == $language) {
      unset($path[0]);
    }
    // - join paths.
    $path = "/" . implode("/", $path);

    // - get block's visibility conditions.
    $config = Drupal::config('qs_coming_soon_alert.settings');
    $limit_pages = $config->get('limit_to_pages');
    $negate = $config->get('limit_to_pages_negate');

    $match = Drupal::service('path.matcher')->matchPath($path, $limit_pages);
    if ($match && $negate == 'show') {
      return TRUE;
    }
    else {
      if (!$match && $negate == 'hide') {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }

}
